Name:         perl-Math-BigRat
Version:      0.2624
Release:      1
Summary:      Arbitrary big rational numbers
License:      GPL+ or Artistic
URL:          http://search.cpan.org/dist/Math-BigRat/
Source0:      http://www.cpan.org/authors/id/P/PJ/PJACKLAM/Math-BigRat-%{version}.tar.gz
BuildArch:    noarch

BuildRequires:perl-generators perl-interpreter perl(ExtUtils::MakeMaker) >= 6.76 perl(Test::More) >= 0.88 findutils

Requires:     perl(:MODULE_COMPAT_%(perl -V:version | cut -d"'" -f 2))
Requires:     perl(Math::BigInt) >= 1.999821
Conflicts:    perl < 4:5.22.0-348

%description
Math::BigRat complements Math::BigInt and Math::BigFloat by providing support
for arbitrary big rational numbers.

%package_help

%prep
%autosetup -n Math-BigRat-%{version} -p1

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PERLLOCAL=1 NO_PACKLIST=1
%make_build

%install
%make_install
%{_fixperms} -c %{buildroot}

%check
unset AUTHOR_TESTING RELEASE_TESTING
make test

%pre

%preun

%post

%postun

%files
%defattr(-,root,root)
%license LICENSE
%doc BUGS README
%{perl_vendorlib}/*

%files help
%defattr(-,root,root)
%doc CHANGES TODO
%{_mandir}/*/*

%changelog
* Fri Jul 14 2023 wangjiang <wangjiang37@h-partners.com> - 0.2624-1
- upgrade version to 0.2624

* Tue Nov 01 2022 wangjiang <wangjiang37@h-partners.com> - 0.2620-2
- Rebuild for next release

* Fri Dec 17 2021 shangyibin <shangyibin1@huawei.com> - 0.2620-1
- upgrade to 0.2620

* Wed Sep 11 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.2614-2
- Package init
